var files_dup =
[
    [ "imu_driver.py", "imu__driver_8py.html", "imu__driver_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.ShareMotor", "classshares_1_1ShareMotor.html", "classshares_1_1ShareMotor" ],
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Controller", "classtask__controller_1_1Controller.html", "classtask__controller_1_1Controller" ]
    ] ],
    [ "task_imu.py", "task__imu_8py.html", "task__imu_8py" ],
    [ "task_motorFINAL.py", "task__motorFINAL_8py.html", "task__motorFINAL_8py" ],
    [ "task_record.py", "task__record_8py.html", "task__record_8py" ],
    [ "task_touch.py", "task__touch_8py.html", "task__touch_8py" ],
    [ "task_userFINAL.py", "task__userFINAL_8py.html", "task__userFINAL_8py" ],
    [ "touch_driver.py", "touch__driver_8py.html", [
      [ "touch_driver.TouchPanel", "classtouch__driver_1_1TouchPanel.html", "classtouch__driver_1_1TouchPanel" ]
    ] ]
];