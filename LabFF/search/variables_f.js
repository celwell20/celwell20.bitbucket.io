var searchData=
[
  ['s0_5finit_0',['S0_INIT',['../task__record_8py.html#a446100dbebab88af2eac915b5d7308e1',1,'task_record.S0_INIT()'],['../task__userFINAL_8py.html#a6d9a7f1a920c3e66cb7cccddda224a4d',1,'task_userFINAL.S0_INIT()']]],
  ['s1_5fwait_5ffor_5finput_1',['S1_WAIT_FOR_INPUT',['../task__record_8py.html#a5a407e9fb7d1f1557224f38b33aba951',1,'task_record.S1_WAIT_FOR_INPUT()'],['../task__userFINAL_8py.html#a257a73ff9af7610b63465bacad9fb5fd',1,'task_userFINAL.S1_WAIT_FOR_INPUT()']]],
  ['scl_2',['SCL',['../imu__driver_8py.html#a060b4fd65d3d3475c875dfd0a87fdd1b',1,'imu_driver']]],
  ['sda_3',['SDA',['../imu__driver_8py.html#a5c918f48948c3d4322610c58b4a0f8d7',1,'imu_driver']]],
  ['settime_4',['setTime',['../classtask__touch_1_1TouchPanel.html#afba76585f80a7029b7bb9c4fb01b368c',1,'task_touch.TouchPanel.setTime()'],['../classtouch__driver_1_1TouchPanel.html#aafb0f0cd42b594191c4bb47725385397',1,'touch_driver.TouchPanel.setTime()']]],
  ['share_5',['Share',['../classtask__imu_1_1TaskIMU.html#a7c58ec7282f7d63383f6709e13cfe8d5',1,'task_imu.TaskIMU.Share()'],['../classtask__touch_1_1TouchPanel.html#a188599c62304cc9996be02b587ade76c',1,'task_touch.TouchPanel.Share()']]],
  ['shares_6',['Shares',['../classtask__motorFINAL_1_1TaskMotor.html#a4cb7db60a3e9c01272927defc1074a0d',1,'task_motorFINAL::TaskMotor']]],
  ['sleep_7',['SLEEP',['../classmotor__driver_1_1DRV8847.html#ae5db6c4b0d1672c7ed1a58c6863cf54c',1,'motor_driver::DRV8847']]],
  ['state_8',['state',['../classtask__controller_1_1Controller.html#a471b6c77ab6ae08871f58dfceff822de',1,'task_controller.Controller.state()'],['../classtask__record_1_1TaskRecord.html#a3655ff41088a4b9f78a9d7595eea4d5a',1,'task_record.TaskRecord.state()'],['../classtask__userFINAL_1_1TaskUser.html#aa3535aa860b36962c569dbbf61589a40',1,'task_userFINAL.TaskUser.state()']]],
  ['stateshare_9',['stateShare',['../classtask__record_1_1TaskRecord.html#ad74c42757099ab44d8dc5eab545b6b10',1,'task_record.TaskRecord.stateShare()'],['../classtask__userFINAL_1_1TaskUser.html#a953cac379f39cc7532e92e8dccfc248b',1,'task_userFINAL.TaskUser.stateShare()'],['../main_8py.html#a3f8356feea88997e211fd8517d10294b',1,'main.stateShare()']]]
];
