var main__lab3_8py =
[
    [ "CHECK_FAULT", "main__lab3_8py.html#ac91e2a53c35ad6f99174534479b55d31", null ],
    [ "DUTY", "main__lab3_8py.html#a4fc76c22c7054959105bf1c29eb70c92", null ],
    [ "E_N", "main__lab3_8py.html#a309c73e72362d1597757b24ecdb7a4c0", null ],
    [ "FIX_FAULT", "main__lab3_8py.html#a7e48f00722ca78ab1d10e42992bdf569", null ],
    [ "motor_drv", "main__lab3_8py.html#a387fd0336c60ee58778997a59f3136c6", null ],
    [ "motorTask1", "main__lab3_8py.html#ae39cf9f94c59c117edd3aa3d7e6edacf", null ],
    [ "motorTask2", "main__lab3_8py.html#a470583b04685b02383e6364d56689504", null ],
    [ "PID", "main__lab3_8py.html#a76fe701c0a03a915acf3f6cfdff9f890", null ],
    [ "POS", "main__lab3_8py.html#a82d61ce3ff7e840d0920bdf0f727a8bd", null ],
    [ "REF_POS", "main__lab3_8py.html#a7f90ced9a9e84590a84292b4eed9c2e9", null ],
    [ "REF_VEL", "main__lab3_8py.html#aa5dfc47552dffe65f4c959f466ddd00b", null ],
    [ "ShareM1", "main__lab3_8py.html#ac0c46f15774b9f6e27751e6261b2160b", null ],
    [ "ShareM2", "main__lab3_8py.html#af8d73dc3d6ac94175b59de37f7d7874c", null ],
    [ "T_motor", "main__lab3_8py.html#a26387c61b793d8df2916ad1eb0d57ada", null ],
    [ "T_user", "main__lab3_8py.html#ab0de55f055df69719a5741f625ee350c", null ],
    [ "userTask", "main__lab3_8py.html#ab2ea47388d8dab9fced472a32dee88fc", null ],
    [ "VEL", "main__lab3_8py.html#a2ab01a4eca8c16217b0b8873e653acbb", null ],
    [ "ZERO", "main__lab3_8py.html#a06dc53d6f54bb37d333d215b90378d30", null ]
];